import * as http from 'http';

// TODO: Implement this class.
export class Router {
		private routes: Map<string, Handler<any>>;
		
		constructor () {
			this.routes = new Map();
		}

		private getRouteKey(method: string, path: string): string {
			return `${method} ${path}`;
		}

    get<T>(path: string, handler: Handler<T>): this {
			const key = this.getRouteKey('GET', path);
			if (this.routes.has(key)) {
				throw new Error('Handler already exists for GET ${path}');
			}
			this.routes.set(key, handler);
			return this;
    }

    post<T>(path: string, handler: Handler<T>): this {
			const key = this.getRouteKey('POST', path);
			if (this.routes.has(key)) {
				throw new Error('Handler already exists for GET ${path}');
			}
			this.routes.set(key, handler);
			return this
    }

    put<T>(path: string, handler: Handler<T>): this {
			const key = this.getRouteKey('PUT', path);
			if (this.routes.has(key)) {
				throw new Error('Handler already exists for GET ${path}');
			}
			this.routes.set(key, handler);
			return this
    }

    delete<T>(path: string, handler: Handler<T>): this {
			const key = this.getRouteKey('DELETE', path);
			if (this.routes.has(key)) {
				throw new Error('Handler already exists for GET ${path}');
			}
			this.routes.set(key, handler);
			return this
    }

getHandler() {
    return async (request: any, response: any) => {
        const { method, url } = request;
        const key = this.getRouteKey(method, url); // GET /data
        const handler = this.routes.get(key);

        let body = '';
        request.on('data', (chunk: any) => {
            body += chunk.toString();
        });
        request.on('end', () => {
            let parsedBody;
            try {
                parsedBody = JSON.parse(body);
            } catch (error) {
                response.writeHead(400, { 'Content-Type': 'text/plain' });
                response.end('Bad Request: Invalid JSON');
                return;
            }

            request.body = parsedBody;

            if (handler) {
                try {
                    handler(request, response);
                } catch (err) {
										response.statusCode = 500;
                    // response.writeHead(500);
                    response.end();
                }
            } else {
							  // response.statusCode = 404;
                response.writeHead(404);
                response.end();
            }
        });
    };
}
}

export type Handler<T> = (request: any, response: any) => void;

export enum HTTPMethod {
    GET = 'GET',
    POST = 'POST',
    PUT = 'PUT',
    DELETE = 'DELETE'
}

